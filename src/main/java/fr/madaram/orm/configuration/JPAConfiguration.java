package fr.madaram.orm.configuration;

import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.cfg.Environment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import liquibase.resource.ClassLoaderResourceAccessor;

@Configuration
@EnableTransactionManagement
@ComponentScan(JPAConfiguration.PACKAGE_TO_SCAN)
public class JPAConfiguration {

	private static final String LIQUIDE_CONFIG = "liquideBaseConfiguration";
	static final String PACKAGE_TO_SCAN = "fr.madaram.orm";

	/** Hibernate settings, in a property file usually. */
	@Bean
	public Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
		properties.put(Environment.SHOW_SQL, "true");
		/**
		 * Auto export/update schema using hbm2ddl tool. Valid values are
		 * <tt>update</tt>, <tt>create</tt>, <tt>create-drop</tt> and
		 * <tt>validate</tt>.
		 */
		properties.put(Environment.HBM2DDL_AUTO, "validate");
		return properties;
	}

	/** JPA main configuration. */
	@Bean
	@DependsOn(LIQUIDE_CONFIG)
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryContainer(DataSource dataSource,
			Properties hibernateProperties, JpaVendorAdapter jpaVendorAdapter) {
		LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		localContainerEntityManagerFactoryBean.setDataSource(dataSource);
		localContainerEntityManagerFactoryBean.setPackagesToScan(PACKAGE_TO_SCAN);
		localContainerEntityManagerFactoryBean.setJpaProperties(hibernateProperties);
		localContainerEntityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		return localContainerEntityManagerFactoryBean;
	}

	/** Use hibernate for JPA implementation. */
	@Bean
	public JpaVendorAdapter hibernateJpaVendorAdapter() {
		return new HibernateJpaVendorAdapter();
	}

	/** Transactional setting, to be used with aop or @transational. */
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

	/** Memory database. */
	@Bean
	public DataSource dataSource() throws ClassNotFoundException {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriverClass((Class<? extends Driver>) Class.forName("org.h2.Driver"));
		// jdbc:h2:mem:test;DB_CLOSE_DELAY=-1 for pure memory
		// jdbc:h2:./test for file DB
		dataSource.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		return dataSource;
	}

	@Bean(name = LIQUIDE_CONFIG)
	public SpringLiquibase liquibase() throws ClassNotFoundException, SQLException, LiquibaseException {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setChangeLog("classpath:changeLog-master.xml");
		liquibase.setDataSource(dataSource());
		return liquibase;
	}

	// liquibase --driver=org.h2.Driver \
	// --classpath=/c/Users/mdaramy/.m2/repository/com/h2database/h2/1.4.185/h2-1.4.185.jar
	// \
	// --changeLogFile=/d/Users/mdaramy/Desktop/workspace/repos/tutoriaux/orm/liquideBaseWithJpa/src/main/resources/liquibase-changeLog.xml
	// \
	// --url="jdbc:h2:./test" \
	// --username=sa \
	// --password= \
	// generateChangeLog

}
