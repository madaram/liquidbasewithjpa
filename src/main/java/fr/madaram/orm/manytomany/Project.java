package fr.madaram.orm.manytomany;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
public class Project {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROJECT_SEQ")
	@SequenceGenerator(name = "PROJECT_SEQ", sequenceName = "PROJECT_SEQ")
	private final long id;

	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private final Set<ProjectAssociation> employees;

	/** Used by hibernate */
	@SuppressWarnings("unused")
	private Project() {
		this(0, null);
	}

	public Project(long id, Set<ProjectAssociation> employees) {
		super();
		this.id = id;
		this.employees = employees;
	}

	public void addEmployee(Employee employee, boolean isProjectLead) {
		ProjectAssociation association = new ProjectAssociation(employee.getId(), this.getId(), isProjectLead, employee,
				this);
		this.employees.add(association);
		employee.getProjects().add(association);
	}

	public long getId() {
		return id;
	}

	public Set<ProjectAssociation> getEmployees() {
		return employees;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
