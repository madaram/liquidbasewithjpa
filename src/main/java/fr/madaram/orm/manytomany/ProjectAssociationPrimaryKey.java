package fr.madaram.orm.manytomany;

import java.io.Serializable;

public class ProjectAssociationPrimaryKey implements Serializable {

	private static final long serialVersionUID = 7650264098262656531L;

	private long employee;

	private long project;

	public long getEmployee() {
		return employee;
	}

	public void setEmployee(long employee) {
		this.employee = employee;
	}

	public long getProject() {
		return project;
	}

	public void setProject(long project) {
		this.project = project;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employee ^ (employee >>> 32));
		result = prime * result + (int) (project ^ (project >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectAssociationPrimaryKey other = (ProjectAssociationPrimaryKey) obj;
		if (employee != other.employee)
			return false;
		if (project != other.project)
			return false;
		return true;
	}	
}
