package fr.madaram.orm.manytomany;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ProjectBaseRepository implements ProjectRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void add(Project project) {
		entityManager.persist(project);
	}

	@Override
	public void addEmployee(Employee employee) {
		entityManager.persist(employee);
	}

	@Override
	public List<Project> findAllEagerly() {
		CriteriaQuery<Project> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Project.class);
		Root<Project> root = criteriaQuery.from(Project.class);
		Fetch<Project, ProjectAssociation> association = root.fetch(Project_.employees, JoinType.LEFT);
		association.fetch(ProjectAssociation_.employee, JoinType.LEFT).fetch(Employee_.projects, JoinType.LEFT);
		association.fetch(ProjectAssociation_.project, JoinType.LEFT).fetch(Project_.employees, JoinType.LEFT);
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

	@Override
	public List<Project> findAll() {
		CriteriaQuery<Project> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Project.class);
		Root<Project> root = criteriaQuery.from(Project.class);
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

}
