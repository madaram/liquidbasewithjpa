package fr.madaram.orm.manytomany;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@IdClass(ProjectAssociationPrimaryKey.class)
public class ProjectAssociation {

	private final boolean isProjectLead;

	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	@Id
	@JoinColumn(name = "employeeId")
	private final Employee employee;

	@ManyToOne(optional = false)
	@Id
	@JoinColumn(name = "projectId")
	private final Project project;

	/** Used by hibernate */
	@SuppressWarnings("unused")
	private ProjectAssociation() {
		this(0, 0, false, null, null);
	}

	public ProjectAssociation(long employeeId, long projectId, boolean isProjectLead, Employee employee,
			Project project) {
		super();
		this.isProjectLead = isProjectLead;
		this.employee = employee;
		this.project = project;
	}

	public boolean isProjectLead() {
		return isProjectLead;
	}

	public Employee getEmployee() {
		return employee;
	}

	public Project getProject() {
		return project;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((employee == null) ? 0 : employee.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectAssociation other = (ProjectAssociation) obj;
		if (employee == null) {
			if (other.employee != null)
				return false;
		} else if (!employee.equals(other.employee))
			return false;
		if (project == null) {
			if (other.project != null)
				return false;
		} else if (!project.equals(other.project))
			return false;
		return true;
	}

}