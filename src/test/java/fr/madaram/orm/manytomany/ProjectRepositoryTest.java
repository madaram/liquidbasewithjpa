package fr.madaram.orm.manytomany;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.LazyInitializationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.madaram.orm.configuration.JPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JPAConfiguration.class)
public class ProjectRepositoryTest {

	@Autowired
	private ProjectRepository projectRepository;

	@Test
	public void testFindAllEagerly() {
		// given
		Project project = givenProjectWithTwoEmployes();
		// when
		List<Project> projects = projectRepository.findAllEagerly();

		assertThat(projects).contains(project);
		thenDeepEquals(project, projects);
	}

	@Test(expected = LazyInitializationException.class)
	public void testFindAll() {
		// given
		Project project = givenProjectWithTwoEmployes();
		// when
		List<Project> projects = projectRepository.findAll();

		assertThat(projects).contains(project);
		thenDeepEquals(project, projects);
	}

	private void thenDeepEquals(Project project, List<Project> projects) {
		Project returnedProject = projects.stream().filter(x -> x.equals(project)).findFirst().get();
		assertThat(returnedProject.getEmployees()).containsAll(project.getEmployees());
		assertThat(returnedProject.getEmployees().iterator().next().getEmployee())
				.isEqualTo(project.getEmployees().iterator().next().getEmployee());
	}

	private Project givenProjectWithTwoEmployes() {
		Set<ProjectAssociation> projectAssociations = new HashSet<>();
		Project project = new Project(0, projectAssociations);
		Employee employee = new Employee(0, projectAssociations);
		Employee lead = new Employee(0, projectAssociations);

		project.addEmployee(employee, false);
		project.addEmployee(lead, true);

		projectRepository.add(project);

		return project;
	}
}
