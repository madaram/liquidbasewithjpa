package fr.madaram.orm.manytomany;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.madaram.orm.configuration.JPAConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JPAConfiguration.class)
public class ProjectAssociationTest {

	@PersistenceContext
	private EntityManager entityManager;

	@Test
	@Transactional
	public void testMapping() {
		Set<ProjectAssociation> employees = new HashSet<>();
		Project project = new Project(0, employees);

		Set<ProjectAssociation> projects = new HashSet<>();
		Employee employee = new Employee(0, projects);
		Employee lead = new Employee(0, projects);
		project.addEmployee(employee, false);
		project.addEmployee(lead, true);

		entityManager.persist(project);
	}

	@Test
	@Transactional
	public void testFin() {
		assertThat(entityManager.find(Project.class, (long) 1)).isNotNull();
	}

}
